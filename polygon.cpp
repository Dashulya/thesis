#include "polygon.h"
#include <QtMath>

Polygon::Polygon(QVector<QPoint> c, QVector2D velocity, QVector2D centre)
    : Figure(centre, velocity), coordinates(c), normals(QVector<QVector2D>())
{
    int r = 0, buf;
    for(int i = 0; i < c.size(); i++) {
        buf = qPow(c[i].x(), 2) + qPow(c[i].y(), 2);
        if(r < buf) r = buf;
    }
    setBoundRadius(sqrt(r));
    setMass();
    setNormals();
    minX = minY = 1000;
    maxX = maxY = 0;
    for(int i = 0; i < c.size(); i++) {
        if(c[i].x() > maxX)
            maxX = c[i].x();
        if(c[i].x() < minX)
            minX = c[i].x();

        if(c[i].y() > maxY)
            maxY = c[i].y();
        if(c[i].y() < minY)
            minY = c[i].y();
    }

}

QVector<QPoint> Polygon::getCoordinates()
{
    QVector<QPoint> buf;
    for(int i = 0; i < coordinates.size(); i++)
        buf.append(QPoint(coordinates[i].x() + center.x(),
                          coordinates[i].y() + center.y()));
    return buf;

}

float Polygon::getmaxX()
{
    return maxX + center.x();
}

float Polygon::getminX()
{
    return minX + center.x();
}

float Polygon::getmaxY()
{
    return maxY + center.y();
}

float Polygon::getminY()
{
    return minY + center.y();
}

inline float Polygon::area(QPoint a, QPoint b)
{
    return abs((a.x() - center.x()) * (b.y() - center.y()) - (b.x() - center.x()) * (a.y() - center.y()));
}

void Polygon::setMass()
{
    float s = 0;
    for(int i=0; i<coordinates.size()-1; i++)
        s+= area(coordinates[i], coordinates[i+1]);
    mass = s*0.5;
}

void Polygon::setNormals()
{
    QVector2D buf;
    int size = coordinates.size() - 1;
    for(int i = 0; i < size; i++)
    {
        normals.append(QVector2D(-coordinates[i].y() + coordinates[i+1].y(),
                                  coordinates[i].x() - coordinates[i+1].x()));
        normals[i].normalize();
    }
    normals.append(QVector2D(-coordinates[size].y() + coordinates[0].y(),
                              coordinates[size].x() - coordinates[0].x()));
    normals[size].normalize();

//    for(int i = 0; i < size; i++ ) {
//        normals.append(QVector2D(normals[i].x() + normals[i + 1].x(),
//                                 normals[i].y() + normals[i + 1].y()));
//        normals[i + size + 1].normalize();
//    }
//    normals.append(QVector2D(normals[size].x() + normals[0].x(),
//                             normals[size].y() + normals[0].y()));
//    normals[size * 2 + 1].normalize();

}
