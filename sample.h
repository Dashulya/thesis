#ifndef SAMPLE_H
#define SAMPLE_H
#include <QPixmap>
#include <QVector>
#include <QVector2D>
class Sample
{
    int type;
    int radius;
    QVector<QPoint> coordinates;
    QPixmap map;
public:
    Sample() : type(-1), radius(-1) {}
    Sample(int radius, QPixmap map);
    Sample(QVector<QPoint> coordinates, QPixmap map);
    QPixmap getMap() { return map;}
    int getType() {return type;}
    QVector<QPoint> getCoordinates() { return coordinates;}
};

#endif // SAMPLE_H
