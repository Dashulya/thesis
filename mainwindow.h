#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QEvent>
#include "panel.h"
#include "simulation.h"
#include "sample.h"
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QBasicTimer>
#include <QSlider>
#include <QFile>
#include <QTextStream>
#include <QThread>
#include <QGroupBox>
#include <QSpinBox>
#include "objectfinder.h"
class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

private slots:
    void on_next_pressed();
    void on_previous_pressed();
//    void modeChanged(bool);
    void addFigure(QVector2D pos);
    void panelSizeChanged(QSize size);
    void on_play_pressed();
    void generateObjects();
    void clearScene();
    void newSizeValue(int size);
    void EtlWasSetted();
    void updateMainMap();
    void writeTime(long time);

signals:
    void sendImage(QPixmap pixmap);
    void mapSizeChanged(QSize size);
    void startStep(bool flag);
    void sendEtl(QPixmap pixmap);
private:
    int numberOfSample;
    bool isPause;
    bool needDrawBuf;
    int dt;
    int stepForView;


    Panel *panel;
    QWidget *menu;
    QPushButton *play_pause;
    QGroupBox *settings;
    QSlider *rate;
    QSpinBox *spinRate;
    QPushButton *clear;
    QGroupBox *addObject;
    QLabel *speed_label;
    QSpinBox *speed_spinBox;
    QLabel *size_label;
    QSpinBox *size_spinBox;
    Panel *pattern;
    QGroupBox *generate;
    QSpinBox *count_spinBox;
    QPushButton *add;
    QPushButton *next;
    QPushButton *previous;
    QCheckBox *viewTarget;

    QVector <Sample> patterns;
    QPixmap mainMap;
    Simulation* simulator;
    ObjectFinder* finder;
    QFile _file;
    QFile timeFile;
    QTextStream stream;
    QTextStream timestream;

    QBasicTimer timer;
    QThread *simThread;
    QThread *finderThread;

    Sample getCircle();
    Sample getPoligon(int vertexCount);
    void setAlpha(QPixmap &map);
    QVector<QPoint> getPoliginCoordinates(int count, int r);
    QPixmap getEtlPixmap(int i);
protected:
    void timerEvent(QTimerEvent *event);
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
