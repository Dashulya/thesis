﻿#ifndef SIMULATION_H
#define SIMULATION_H
#include "circle.h"
#include "polygon.h"
#include <QPixmap>
#include <QVector3D>


struct Data {
    int i;
    int j;
    float dif;
    QVector2D normal;

    Data(){}
    Data(int i, int j, float dif, QVector2D normal):
        i(i), j(j), dif(dif), normal(normal) {}
};

struct Shift {
    double value;
    QVector2D normal;

    Shift(){}
    Shift(double val) :
        value(val) {}
    void setNormal(QVector2D normal) { this->normal = normal;}
};

class Simulation: public QObject
{
    Q_OBJECT

    static constexpr float MAXSPEED = 300.f;

    int counter;
    int responceCounter;
    QVector <QVector <Shift> > shiftVectors;
    QVector <QVector2D> listOfCollisions;
    QVector <Data> collisions;
    QVector <Figure*> figures;
    float dt;
    int gridStep;
    QVector <QVector <QVector<int> > > grid;
    QVector<int> checkList;
    QPixmap image;
    QPixmap bufImage;
    QPixmap recImage;
    QVector<QPoint> listOfTatget;

    void checkBound(int l, int k);
    void checkBoundTop(int l, int k);
    void checkBoundBottom(int l, int k);
    void checkBoundRight(int l, int k);
    void checkBoundRightBottom(int l, int k);
    bool check(int i, int j);    
    void checkBorder(int i);
    float getDif(int minA, int maxA, int minB, int maxB);
    float getDif(float difT, float difL);

    void getNewSpeed(Figure &a, Figure &b, QVector2D normal);
    void setShiftNormal(QPoint ca, QPoint cb, QVector2D normal, int ia, int jb);
    void checkShift(Figure* ca, Figure* cb,  Shift &a, Shift &b);
    void checkPosition(Figure &f);
    QVector<QVector2D> getNormals(QVector<QPoint> ver, QPoint center);
    void drawRecImage();
    bool drawTarget;

public:

    Simulation(): counter(0), responceCounter(0), drawTarget(true) {}
    ~Simulation();
    void setGrid();
    void addFigure(Circle *f);
    void addFigure(Polygon *f);
    void checkCollision(Circle &a, Circle &b, int ia, int jb);
    void checkCollision(Polygon& a, Polygon& b, int ia, int jb);
    void checkCollision(Polygon& a, Circle& b, int ia, int jb);
    void checkFigures(Figure &a, Figure &b, int i, int j);
    void setGridStep();

    int getStep() { return counter; }
    int responceCount() { return responceCounter; }

    QPixmap getImage() {return image;}
    QPixmap getRecImage() { return recImage;}
    QVector2D findNormal();
    QVector <Figure*> getFigures() { return figures;}
    void setImageSize(QSize size);
    void setDT(float dt) { this->dt = dt / 1000;}
    void drawBufPixmap(bool flag);

    void clear();

public slots:
    void resetTargets(QVector<QPoint> vec);
    void changeMapSize(QSize size);
    void runStep(bool needDraw);
    void setTargetViewState(int i);

signals:
    void updatePanel();

};

#endif // SIMULATION_H
