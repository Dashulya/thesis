#ifndef FIGURE_H
#define FIGURE_H
#include <QVector2D>
#include <QColor>

class Figure
{
protected:
    QVector2D velocity;
    float mass;
    QVector2D center;
    int boundRadius;
    QColor color;
public:
    Figure(QVector2D center, QVector2D velocity)
        : velocity(velocity), center(center), color(qrand() % 255, qrand() % 255, qrand() % 255) {}
    virtual ~Figure(){}
    virtual int getType() { return 0;}
    float getMass(){ return mass;}
    float getCentreX() { return center.x();}
    float getCentreY() { return center.y();}
    float getVelocityX() {return velocity.x();}
    float getVelocityY() {return velocity.y();}
    float getVelocity();
    void changePosition(float dt);
    void changePosition(float dx,float dy);
    int getBoundRadius() { return boundRadius;}
    void setBoundRadius(float radius) { boundRadius = radius;}
    void setVelocityX(float x) { velocity.setX(x);}
    void setVelocityY(float y) { velocity.setY(y);}
    QPoint getCentre() { return QPoint(center.x(), center.y());}
    void setVelocity(QVector2D velocity) { this->velocity = velocity;}
    QColor getColor() { return color; }
    void setCenterX(int x) { center.setX(x);}
    void setCenterY(int y) { center.setY(y);}
};

#endif // FIGURE_H
