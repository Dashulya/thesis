#include "objectfinder.h"
#include <QDebug>
#include <QBitmap>
#include <QBuffer>
#include <QRgb>
#include <time.h>

void ObjectFinder::getClusters(Cluster & cluster)
{
    //set max & min
    int minpix=10, maxpix=1000;

    Cluster *ClA;

    try
    {
        cluster.Clear();
        cluster.ScanImage(&buf);

        ClA=&cluster;
        while(1)
        {
            ClA=ClA->GetNextCluster();
            if(ClA==NULL)
                break;
            if(ClA->GetPixCount()<minpix)
            {
                ClA->~Cluster();
            }
            else if(ClA->GetPixCount()>maxpix)
            {
                ClA->~Cluster();
            }
        }

        cluster.AllCalculateCMass();
        cluster.AllToPol();
        cluster.AllPolarSort();
        cluster.AllInterDegNew(360);

        cluster.AllNorm(360);
    }
    catch(...)
    {
        qDebug() << "From getCluster";
    }

}

void ObjectFinder::GeomCorr()
{
    // set dif
    double thres = 10;

    int i, k;
    double sum, minsum=99999., fi=0., temp;

    Cluster *ClA=&Cl, *ClB, *Et=&Etl;

    try {

        ClA=ClA->GetNextCluster();
        Et=Et->GetNextCluster();

        while(ClA!=NULL)
        {
            minsum =99999. ;
            for(k=0; k<360; ++k)
            {
                sum=0;
                for(i=0; i<360; ++i)
                {
                    sum+=( ClA->GetR360(i)*100 - Et->GetR360(i)*100 )  *
                            ( ClA->GetR360(i)*100 - Et->GetR360(i)*100 );
                }
                sum/=360;
                sum=sqrt(sum);
                if
                        ( sum < minsum ) { minsum = sum; fi=(double)k; }

                temp=ClA->GetR360(0);
                for (int m = 1; m < 360; ++m)
                    ClA->SetR360(m-1,ClA->GetR360(m));
                ClA->SetR360(359,temp);
            }
            //Если больше то выбрасываем, то есть записываем в ...
            if( minsum > thres)            // /100.
            {
                ClB=ClA;
                ClA=ClA->GetNextCluster();
                ClB->~Cluster();
            }
            else
            {
                ClA=ClA->GetNextCluster();
            }
        }
    }
    catch(...)
    {
        qDebug() << "From GeomCorr";
    }
}

void ObjectFinder::getContours()
{
    QRgb *ptr;

    quint8 *TempBuffer0=new quint8[buf.width()*3];
    quint8 *TempBuffer1=new quint8[buf.width()*3];
    quint8 *TempBuffer2=new quint8[buf.width()*3];

    memset(TempBuffer0, 0, sizeof(buf.width()*3));
    memset(TempBuffer1, 0, sizeof(buf.width()*3));

    try
    {
        for(int y=1; y<buf.height(); y++)
        {
            ptr = (QRgb*)buf.scanLine(y);
            
            for(int ii = 0; ii < buf.width(); ii++) {
                TempBuffer2[ii * 3] = qRed(ptr[ii]);
                TempBuffer2[ii * 3 + 1] = qGreen(ptr[ii]);
                TempBuffer2[ii * 3 + 2] = qBlue(ptr[ii]);
            }

            for(int x=0; x<buf.width(); x++)
            {
                if(TempBuffer1[x*3] == 255)
                {
                    if( ( TempBuffer0[(x+0)*3] == 255) && (TempBuffer1[(x+1)*3] == 255)&&
                            ( TempBuffer2[(x+0)*3] == 255) && (TempBuffer1[(x-1)*3] == 255) )
                    {
                        ptr[x] = qRgb(0, 0, 0);
                    }
                    else
                    {
                        ptr[x] = qRgb(255, 255, 255);
                    }
                    continue;
                }  //end if
                else
                {
                    ptr[x] = qRgb(0, 0, 0);
                }
            } //end for x
            //--------- change string in bufer ---------------------
            for(int jj=0; jj<buf.width()*3; jj++)
            {
                TempBuffer0[jj]=TempBuffer1[jj];
                TempBuffer1[jj]=TempBuffer2[jj];
            }
        }
    }
    catch(...)
    {
        qDebug() << "From get Contour";
    }

    delete[] TempBuffer0;
    delete[] TempBuffer1;
    delete[] TempBuffer2;
}

void ObjectFinder::recognize(QPixmap map)
{
    _donotsend = false;
    buf = map.toImage();
    long start = clock();
    getContours();
    getClusters(Cl);
    GeomCorr();
    long time = clock() - start;
    QVector<QPoint> targets;

    Cluster *ClA=&Cl;
    ClA = ClA->GetNextCluster();
    while (ClA != NULL) {
        ClA->GetPixCount();
        targets.append(ClA->getCMass());

        qDebug() << "cluster";
        Pixel* CurrP=ClA->PFirst;
        while(CurrP!=NULL)
        {
            qDebug() << CurrP->Coords[0] << CurrP->Coords[1];
            CurrP=CurrP->Next;
        }

        ClA = ClA->GetNextCluster();

        qDebug() << targets[targets.size() - 1];
        qDebug() << "---------------------------";

    }


    emit sendtime(time / CLOCKS_PER_SEC);
    if(!_donotsend)
        emit sendMark(targets);
}

ObjectFinder::ObjectFinder(QObject *parent) :
    QObject(parent)
{
    //    connect(&worker, SIGNAL(started()),
    //            this, SLOT(recognize()));
    //    connect(&worker, SIGNAL(finished()),
    //            this, SLOT(workerAvailable()));

}

void ObjectFinder::setEtl(QPixmap etl)
{
    buf = etl.toImage();
    getContours();
    getClusters(Etl);
    emit EtlIsWasSetted();
}

//void ObjectFinder::addImage(QPixmap map)
//{
//    Imagequeue.append(map);
//    if(!worker.isRunning())
//        workerAvailable();

//}

//void ObjectFinder::workerAvailable()
//{
//    if(Imagequeue.size() != 0) {
//        buf = Imagequeue.takeFirst();
//        worker.start();
//    }
//}

