#include "speed.h"
#include <cmath>

using namespace std;

Speed::Speed()
    :x(0), y(0), value(0)
{
}

Speed::Speed(int x, int y)
    :x(x), y(y)
{
    value = sqrt(pow((double)x, 2)+pow((double)y, 2));
}

void Speed::updateValue()
{
    value = sqrt(pow((double)x, 2)+pow((double)y, 2));
}
