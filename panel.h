#ifndef PANEL_H
#define PANEL_H

#include <QWidget>
#include <QPaintEvent>
#include <QPixmap>

class Panel : public QWidget
{
    Q_OBJECT
private:
    QPixmap map;
    QPixmap dragMap;
    bool dropEnabled;
    int type;

public:
    explicit Panel(bool enabled, int type, QWidget *parent = 0);
    void setMap(const QPixmap &m) { map = m; update();}
    void setDropEnavled(bool enabled);
    int getType() { return type;}
    void setDragMAp(QPixmap pixmap) {dragMap = pixmap;}


protected:
    void paintEvent(QPaintEvent *);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);

signals:
    void addFigure(QVector2D p);
    void sizeChanged(QSize size);
    void setEnableTrue();

};

#endif // PANEL_H
