#include "mainwindow.h"
#include "sample.h"
#include <QGraphicsScene>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QFormLayout>
#include <QPainter>
#include <qmath.h>
#include <QDebug>
#include <QtMath>
#include <ctime>

#ifdef FAST_DEBUG
bool PAUSE_ON_STEP = true;
#else
bool PAUSE_ON_STEP = true;
#endif

//QString writeToFile(int count)
//{
//    static QFile file("steps.log");
//    static QTextStream stream;
//    if(!file.isOpen())
//    {
//        file.open(QFile::WriteOnly | QFile::Text);
//        stream.setDevice(&file);
//    }

//    QString text = QString::number(count);
//    if(file.isOpen())
//        stream << text << endl;
//    return text;
//}

MainWindow::MainWindow() :
    isPause(true), needDrawBuf(true), simulator(new Simulation()), finder(new ObjectFinder())
{
    simThread = new QThread(0);
    finderThread = new QThread(0);

    panel = new Panel(true, 0, this);
    panel->setMinimumSize(450, 386);
    menu = new QWidget(this);
    menu->setMaximumWidth(180);
    settings = new QGroupBox("Период генерации", menu);
    addObject = new QGroupBox("Скорость объекта", menu);
    generate = new QGroupBox("Добавить произвольные \nфигуры", menu);
    pattern = new Panel(false, 1, addObject);
    pattern->setMaximumSize(140, 140);
    pattern->setMinimumSize(140, 140);
    play_pause = new QPushButton("Начать/пауза", menu);
    play_pause->setEnabled(false);
    add = new QPushButton("Добавить", generate);
    clear = new QPushButton("Новая сцена", menu);
    menu->setMinimumWidth(180);
    menu->setMaximumWidth(180);
    rate = new QSlider(settings);
    rate->setMinimum(10);
    rate->setMaximum(1000);
    rate->setSingleStep(10);
    rate->setPageStep(50);
    rate->setValue(100);
    rate->setOrientation(Qt::Horizontal);
    spinRate = new QSpinBox(settings);
    spinRate->setMinimum(10);
    spinRate->setMaximum(1000);
    spinRate->setSingleStep(10);
    spinRate->setValue(100);
    speed_label = new QLabel("Скорость", addObject);
    size_label = new QLabel("Размер", addObject);
    speed_spinBox = new QSpinBox(addObject);
    speed_spinBox->setMinimum(50);
    speed_spinBox->setMaximum(300);
    size_spinBox = new QSpinBox(addObject);
    size_spinBox->setMinimum(10);
    size_spinBox->setMaximum(30);
    count_spinBox = new QSpinBox(generate);
    count_spinBox->setMinimum(1);
    count_spinBox->setMaximum(100);
    viewTarget = new QCheckBox("Показать цели", menu);
    viewTarget->setCheckState(Qt::Checked);

    next = new QPushButton("->", addObject);
    previous = new QPushButton("<-", addObject);





    connect(rate, SIGNAL(valueChanged(int)),
            spinRate, SLOT(setValue(int)));
    connect(spinRate, SIGNAL(valueChanged(int)),
            rate, SLOT(setValue(int)));
    connect(next, SIGNAL(clicked()),
            this, SLOT(on_next_pressed()));
    connect(previous, SIGNAL(clicked()),
            this, SLOT(on_previous_pressed()));
    connect(panel, SIGNAL(addFigure(QVector2D)),
            this, SLOT(addFigure(QVector2D)));
    connect(panel, SIGNAL(sizeChanged(QSize)),
            this, SLOT(panelSizeChanged(QSize)));
    connect(play_pause, SIGNAL(clicked()),
            this, SLOT(on_play_pressed()));
    connect(finder, SIGNAL(sendMark(QVector<QPoint>)),
            simulator, SLOT(resetTargets(QVector<QPoint>)));
    connect(generate, SIGNAL(clicked()),
            this, SLOT(generateObjects()));
    connect(this, SIGNAL(mapSizeChanged(QSize)),
            simulator, SLOT(changeMapSize(QSize)));
    connect(this, SIGNAL(startStep(bool)),
            simulator, SLOT(runStep(bool)));
    connect(this, SIGNAL(sendEtl(QPixmap)),
            finder, SLOT(setEtl(QPixmap)));
    connect(this, SIGNAL(sendImage(QPixmap)),
            finder, SLOT(recognize(QPixmap)));
    connect(clear, SIGNAL(clicked()),
            this, SLOT(clearScene()));
    connect(size_spinBox, SIGNAL(valueChanged(int)),
            this, SLOT(newSizeValue(int)));
    connect(add, SIGNAL(clicked()),
            this, SLOT(generateObjects()));    
    connect(finder, SIGNAL(EtlIsWasSetted()),
            this, SLOT(EtlWasSetted()));
    connect(panel, SIGNAL(setEnableTrue()),
            this, SLOT(EtlWasSetted()));
    connect(viewTarget, SIGNAL(stateChanged(int)),
            simulator, SLOT(setTargetViewState(int)));
    connect(simulator, SIGNAL(updatePanel()),
            this, SLOT(updateMainMap()));
    connect(finder, SIGNAL(sendtime(long)),
            this, SLOT(writeTime(long)));

    QVBoxLayout *set = new QVBoxLayout(settings);
    set->addWidget(rate);
    set->addWidget(spinRate);
    settings->setLayout(set);

    QVBoxLayout *newObj = new QVBoxLayout;
    QGridLayout *objsettings = new QGridLayout;
    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(previous);
    buttonLayout->addWidget(next);
    objsettings->addWidget(speed_label, 0, 0);
    objsettings->addWidget(speed_spinBox, 0, 1);
    objsettings->addWidget(size_label, 1, 0);
    objsettings->addWidget(size_spinBox, 1, 1);
    newObj->setContentsMargins(0, 9, 0, 0);
    newObj->addLayout(objsettings);
    newObj->addWidget(pattern);
    newObj->addLayout(buttonLayout);
    addObject->setLayout(newObj);

    QVBoxLayout *gen = new QVBoxLayout(generate);
    gen->addWidget(count_spinBox);
    gen->addWidget(add);
    generate->setLayout(gen);

    QVBoxLayout *panelMenu = new QVBoxLayout(menu);
    QSpacerItem *spase1 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);
    QSpacerItem *spase2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    panelMenu->addWidget(play_pause);
    panelMenu->addWidget(settings);
    panelMenu->addWidget(viewTarget);
    panelMenu->addItem(spase1);
    panelMenu->addWidget(clear);
    panelMenu->addWidget(addObject);
    panelMenu->addWidget(generate);
    panelMenu->addItem(spase2);
    menu->setLayout(panelMenu);

    QHBoxLayout *mainWindow = new QHBoxLayout(this);
    mainWindow->addWidget(panel);
    mainWindow->addWidget(menu);

    setLayout(mainWindow);

    patterns.append(getCircle());
    patterns.append(getPoligon(3));
    patterns.append(getPoligon(4));
    patterns.append(getPoligon(5));
    patterns.append(getPoligon(6));
    numberOfSample = 0;
    pattern->setMap(patterns[numberOfSample].getMap());
    mainMap = QPixmap(panel->width(), panel->height());
    mainMap.fill(Qt::white);
    panel->setMap(mainMap);
    simulator->setImageSize(panel->size());
    newSizeValue(10);
    stepForView = 0;
}

MainWindow::~MainWindow()
{
}

//void MainWindow::modeChanged(bool editingMode)
//{
//    if(editingMode)
//    {
//        pattern->setDropEnavled(false);
//    } else
//    {
//        pattern->setDropEnavled(true);
//    }
//}

void MainWindow::on_next_pressed()
{

    numberOfSample += 1;
    if(numberOfSample == patterns.size()) numberOfSample = 0;
    pattern->setMap(patterns[numberOfSample].getMap());
    newSizeValue(size_spinBox->value());
    play_pause->setEnabled(false);
    emit sendEtl(getEtlPixmap(numberOfSample));

}

void MainWindow::on_previous_pressed()
{
    numberOfSample -= 1;
    if(numberOfSample < 0) numberOfSample = patterns.size() - 1;
    pattern->setMap(patterns[numberOfSample].getMap());
    newSizeValue(size_spinBox->value());
    play_pause->setEnabled(false);
    emit sendEtl(getEtlPixmap(numberOfSample));
}

void MainWindow::setAlpha(QPixmap& map)
{
    QColor c;
    QImage image(map.toImage().convertToFormat(QImage::Format_ARGB32));
    for(int i = 0; i < image.width(); i++) {
        for(int j = 0; j < image.height(); j++) {
            c = QColor(image.pixel(i, j));
            if(c == Qt::white) {
                c.setAlpha(0);
                image.setPixel(i, j, c.rgba());
            }
        }
    }
    map = QPixmap::fromImage(image);
}

void MainWindow::updateMainMap()
{
    QPainter p(&mainMap);
    p.drawPixmap(0, 0, mainMap.width(), mainMap.height(), simulator->getImage());
    p.end();
    panel->setMap(mainMap);
}

void MainWindow::writeTime(long time)
{
    timestream << time << endl;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer.timerId()) {
        if(stepForView == dt) {
            stepForView -= dt;
            if(viewTarget->isChecked())
                emit sendImage(simulator->getRecImage());
            emit startStep(true);
        } else {
            emit startStep(false);
        }
        stepForView++;
#ifndef FAST_DEBUG
        updateMainMap();
#endif

//        int step = simulator->getStep();

        stream << (simulator->getStep() - simulator->responceCount()) << endl;

//        if(PAUSE_ON_STEP && step == 2240)
//            qDebug() << "HERE";

//        labelStep->setText(writeToFile(step));
//        countResponce->setText(QString::number(simulator->responceCount()));
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    simThread->quit();
    finderThread->quit();
    event->accept();
}

Sample MainWindow::getCircle()
{

    QPixmap map(this->pattern->width(), this->pattern->height());
    map.fill(Qt::white);
    QPainter p(&map);
    p.setPen(QPen(Qt::black,1,Qt::SolidLine));
    p.drawEllipse(QPoint(map.width() / 2, map.height() / 2), map.width() / 2 - 10, map.width() / 2 - 10 );
    p.end();
    setAlpha(map);
    qDebug() << map.width();
    return Sample(map.width() / 2 - 10, map);
}

QVector<QPoint> MainWindow::getPoliginCoordinates(int count, int r) {
    float step = 2 * M_PI / count;
    float phi = 0;
    QVector<QPoint> coordinates;
    for(int i = 0; i < count; i++)
    {
        coordinates.append(QPoint(qCos(phi) * r,qSin(phi) * r));
        phi += step;
    }
    return coordinates;
}

QPixmap MainWindow::getEtlPixmap(int i)
{
    QPixmap map(100, 100);
    map.fill(Qt::white);
    QPainter p(&map);
    p.setPen(QPen(Qt::black, 1, Qt::SolidLine));
    p.setBrush(Qt::black);
    if(i == 0) {
        p.drawEllipse(QPoint(50, 50), 40, 40 );
    } else {
        QVector<QPoint> coord = getPoliginCoordinates(i +2, 40);
        for(int i = 0; i < coord.size(); i++) {
            coord[i].setX(coord[i].x() + 50);
            coord[i].setY(coord[i].y() + 50);
        }
        QPolygon poligon(coord);
        p.drawPolygon(poligon);
    }
    return map;
}

Sample MainWindow::getPoligon(int vertexCount)
{
    QPixmap map(this->pattern->width(), this->pattern->height());
    map.fill(Qt::white);
    QPainter p(&map);
    p.setPen(QPen(Qt::black,1,Qt::SolidLine));
    int r = map.width() / 2 - 10;
    float step = 2 * M_PI / vertexCount;
    float phi = 0;
    int shiftX = map.width() / 2,
            shiftY = map.height() / 2;
    QVector<QPoint> coordinates;
    for(int i = 0; i < vertexCount; i++)
    {
        coordinates.append(QPoint(qCos(phi) * r,qSin(phi) * r));
        phi += step;
        p.drawLine(coordinates[i].x() + shiftX, coordinates[i].y() + shiftY,
                   qCos(phi) * r + shiftX,qSin(phi) * r + shiftY);
    }
    p.end();
    setAlpha(map);

    return Sample(coordinates, map);
}

void MainWindow::addFigure(QVector2D pos)
{
    play_pause->setEnabled(true);
    double alpha = qrand()%360;
    float x = speed_spinBox->value() * qCos(alpha*M_PI/360);
    float y = speed_spinBox->value() * qSin(alpha*M_PI/360);
    if(patterns[numberOfSample].getType() == 1)
        simulator->addFigure(new Circle(pos, QVector2D(x, y), size_spinBox->value()));
    else
        simulator->addFigure(new Polygon(getPoliginCoordinates(numberOfSample + 2, size_spinBox->value()),
                                         QVector2D(x, y), pos));
    simulator->drawBufPixmap(false);
    updateMainMap();
}

void MainWindow::panelSizeChanged(QSize size)
{
    emit mapSizeChanged(size);
    mainMap = QPixmap(size);
    updateMainMap();
}

void MainWindow::on_play_pressed()
{
    if(needDrawBuf) {
        simulator->drawBufPixmap(true);
        needDrawBuf = !needDrawBuf;
    }
    isPause = !isPause;
    if(!isPause) {
        if(!_file.isOpen())
        {
            int obj = simulator->getFigures().size();
            int rrr = rate->value();
            _file.setFileName(QString("%1_%2.csv").arg(obj).arg(rrr));
            _file.open(QFile::Text | QFile::WriteOnly);
            stream.setDevice(&_file);
        }

        if(!timeFile.isOpen())
        {
            int obj = simulator->getFigures().size();
            int rrr = rate->value();
            timeFile.setFileName(QString("%1_%2_time.csv").arg(obj).arg(rrr));
            timeFile.open(QFile::Text | QFile::WriteOnly);
            timestream.setDevice(&timeFile);
        }

        dt = spinRate->value() / 10;
        simulator->moveToThread(simThread);
        simulator->setDT(10);
        finder->moveToThread(finderThread);
        emit sendEtl(getEtlPixmap(numberOfSample));
        timer.start(10, this);
        finderThread->start();
        simThread->start();
        clear->setEnabled(false);
    } else {
        timer.stop();
        clear->setEnabled(true);
    }
}
#include <ctime>
void MainWindow::generateObjects()
{
    qsrand(std::clock());

    int count = count_spinBox->value();
    for(int i = 0; i < count; i++) {

        int radius;
        int random = qrand();
        if(random % 100 < 25)
            radius = (qrand() % 5) + 10;
        else if(random % 100 < 70)
            radius = (qrand() % 4) + 6;
        else
            radius = (qrand() % 5) + 25;

        int x = 0;
        int y = 0;

        bool wrong;
        int counts = 0;
        do
        {
            wrong = false;
            x = (qrand() % (mainMap.width()  - 2*radius)) + radius;
            y = (qrand() % (mainMap.height() - 2*radius)) + radius;

            auto figures = simulator->getFigures();

            for(Figure* f : figures)
            {
                double distance = qPow((f->getCentreX() - x), 2.0) + qPow((f->getCentreY() - y), 2.0);
                if(distance < qPow(radius + f->getBoundRadius(), 2.0))
                {
                    wrong = true;
                    break;
                }
            }
            counts++;
        }
        while(wrong && (counts < 200));

        if(counts < 200)
        {
            int type = qrand() % 10;
            type = type % 2 == 0 ? 2 : 1;
            double alpha = qrand()%360;
            float speed = qrand()%250 + 50;
            float spX = speed * qCos(alpha*M_PI/360);
            float spY = speed * qSin(alpha*M_PI/360);
            if(type == 1) {
                Circle *newF = new Circle(QVector2D(x, y), QVector2D( spX, spY), radius);
                simulator->addFigure(newF);
            } else {
                int verCount = qrand() % 3 + 3;
                Polygon *newF = new Polygon(getPoliginCoordinates(verCount, radius), QVector2D( spX, spY), QVector2D(x, y));
                simulator->addFigure(newF);
            }

            qDebug() << "Add count:" << i << "Figure:" << x << y << spX << spY << radius << "Type" << type;

        }
    }
    updateMainMap();
    play_pause->setEnabled(true);
    qDebug() << "--------------";
    qDebug() << "Total added" << simulator->getFigures().size();
}

void MainWindow::clearScene()
{
    finder->doNotSend();
    finderThread->exit();
    simulator->clear();
    mainMap.fill(Qt::white);
    _file.close();
    timeFile.close();
    updateMainMap();
    stepForView = 0;
}

void MainWindow::newSizeValue(int size)
{
    QPixmap map(140, 140);
    map.fill(Qt::white);
    QPainter p(&map);
    p.setPen(QPen(Qt::black,1,Qt::SolidLine));
    if(numberOfSample == 0) {
        p.drawEllipse(QPoint(70, 70), size, size);
    } else {
        QVector<QPoint> coord = getPoliginCoordinates(numberOfSample + 2, size);
        for(int i = 0; i < coord.size(); i++) {
            coord[i].setX(coord[i].x() + 70);
            coord[i].setY(coord[i].y() + 70);
        }
        QPolygon poligon(coord);
        p.drawPolygon(poligon);
    }
    p.end();
    setAlpha(map);
    pattern->setDragMAp(map);

}

void MainWindow::EtlWasSetted()
{
    play_pause->setEnabled(true);
}

