#ifndef OBJECTFINDER_H
#define OBJECTFINDER_H

#include <QObject>
#include <QLinkedList>
#include <QPixmap>
#include <QThread>
#include <QString>
#include <QImage>
#include "cluster.h"

class ObjectFinder : public QObject
{
    Q_OBJECT
    QLinkedList<QPixmap> Imagequeue;
    QImage buf;
    //QThread worker;
    Cluster Cl, Etl;

    volatile bool _donotsend;

    void getClusters(Cluster &cluster);
    void GeomCorr();
    void getContours();

public:
    explicit ObjectFinder(QObject *parent = 0);

    void doNotSend() { _donotsend = true; }

private slots:
    //void workerAvailable();


public slots:
//    void addImage(QPixmap map);
    void setEtl(QPixmap etl);
    void recognize(QPixmap map);

signals:
    void sendMark(QVector<QPoint> vec);
    void EtlIsWasSetted();
    void sendtime(long time);


};

#endif // OBJECTFINDER_H
