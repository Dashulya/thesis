#ifndef CIRCLE_H
#define CIRCLE_H
#include"figure.h"

class Circle : public Figure
{
    int radius;
public:
    Circle(QVector2D center, QVector2D speed, int radius);
    ~Circle(){}
    void setRadius(int value) { radius = value;}
    int getRadius() { return this->radius;}
    void setMass();
    int getType() { return 1;}
};

#endif // CIRCLE_H
