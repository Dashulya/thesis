#include "simulation.h"
#include "circle.h"
#include "polygon.h"
#include <QtMath>
#include <QPainter>
#include <QDebug>
#include <QtAlgorithms>

bool test(const Data& a, const Data& b) {return a.dif > b.dif;}

Simulation::~Simulation()
{
    for(int i = 0; i < figures.size(); i++)
        delete figures[i];
}

void Simulation::setGridStep()
{
    gridStep = 1;
    for(int i = 0; i< figures.size(); i++) {
        if(gridStep < figures[i]->getBoundRadius())
            gridStep = figures[i]->getBoundRadius();
    }
    gridStep *= 2;
}

void Simulation::addFigure(Polygon *f)
{
    figures.append(f);
    shiftVectors.clear();
    shiftVectors.resize(figures.size());
    setGridStep();
    listOfTatget.append(f->getCentre());
    drawBufPixmap(false);
}

void Simulation::addFigure(Circle *f)
{
    figures.append(f);
    shiftVectors.clear();
    shiftVectors.resize(figures.size());
    setGridStep();
    listOfTatget.append(f->getCentre());
    drawBufPixmap(false);
}

void Simulation::setGrid()
{
    grid.clear();
    grid.resize(qCeil((double)image.height()/gridStep));
    int size = qCeil((double)image.width()/gridStep);
    for(int i = 0; i < grid.size(); i++)
        grid[i].resize(size);
}

void Simulation::resetTargets(QVector<QPoint> vec)
{
    listOfTatget = vec;
    responceCounter++;
}

void Simulation::changeMapSize(QSize size)
{
    setImageSize(size);
    setGridStep();
    setGrid();
}

bool Simulation::check(int i, int j)
{
    if(qPow(figures[i]->getCentreX()-figures[j]->getCentreX(), 2) +
            qPow(figures[i]->getCentreY()-figures[j]->getCentreY(), 2) <=
            qPow((double)figures[i]->getBoundRadius() + figures[j]->getBoundRadius(), 2))
        return true;
    return false;
}

void Simulation::checkBound(int l, int k)
{
    for(int i = 0; i < grid[l][k].size(); i++)
    {
        for(int j = i; j < grid[l][k].size(); j++) {
            if(i != j)
                if(check(grid[l][k][i],grid[l][k][j]))
                    listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k][j]));
        }

        for(int j = 0; j < grid[l + 1][k].size(); j++) {
            if(check(grid[l][k][i],grid[l + 1][k][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l + 1][k][j]));
        }

        for(int j = 0; j < grid[l + 1][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l + 1][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l + 1][k + 1][j]));
        }

        for(int j = 0; j < grid[l][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k + 1][j]));
        }

        for(int j = 0; j < grid[l - 1][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l - 1][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l - 1][k + 1][j]));
        }
    }
}

void Simulation::checkBoundTop(int l, int k)
{
    for(int i = 0; i < grid[l][k].size(); i++)
    {
        for(int j = i; j < grid[l][k].size(); j++) {
            if(i != j)
                if(check(grid[l][k][i],grid[l][k][j]))
                    listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k][j]));
        }

        for(int j = 0; j < grid[l + 1][k].size(); j++) {
            if(check(grid[l][k][i],grid[l + 1][k][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l + 1][k][j]));
        }

        for(int j = 0; j < grid[l + 1][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l + 1][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l + 1][k + 1][j]));
        }

        for(int j = 0; j < grid[l][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k + 1][j]));
        }

    }
}

void Simulation::checkBoundBottom(int l, int k)
{
    for(int i = 0; i < grid[l][k].size(); i++)
    {
        for(int j = i; j < grid[l][k].size(); j++) {
            if(i != j)
                if(check(grid[l][k][i],grid[l][k][j]))
                    listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k][j]));
        }

        for(int j = 0; j < grid[l][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k + 1][j]));
        }

        for(int j = 0; j < grid[l - 1][k + 1].size(); j++) {
            if(check(grid[l][k][i],grid[l - 1][k + 1][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l - 1][k + 1][j]));
        }
    }
}

void Simulation::checkBoundRight(int l, int k)
{
    for(int i = 0; i < grid[l][k].size(); i++)
    {
        for(int j = i; j < grid[l][k].size(); j++) {
            if(i != j)
                if(check(grid[l][k][i],grid[l][k][j]))
                    listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k][j]));
        }

        for(int j = 0; j < grid[l + 1][k].size(); j++) {
            if(check(grid[l][k][i],grid[l + 1][k][j]))
                listOfCollisions.append(QVector2D(grid[l][k][i], grid[l + 1][k][j]));
        }
    }
}

void Simulation::checkBoundRightBottom(int l, int k)
{
    for(int i = 0; i < grid[l][k].size(); i++)
    {
        for(int j = i; j < grid[l][k].size(); j++) {
            if(i != j)
                if(check(grid[l][k][i],grid[l][k][j]))
                    listOfCollisions.append(QVector2D(grid[l][k][i], grid[l][k][j]));
        }
    }
}

void Simulation::checkFigures(Figure &a, Figure &b, int i, int j)
{
    if(a.getType() == 1)
    {
        if(b.getType() == 1)
            checkCollision(dynamic_cast<Circle&>(a), dynamic_cast<Circle&>(b), i, j);
        else
            checkCollision(dynamic_cast<Polygon&>(b), dynamic_cast<Circle&>(a), j, i);
    } else
    {
        if(b.getType() == 1)
            checkCollision(dynamic_cast<Polygon&>(a), dynamic_cast<Circle&>(b), i, j);
        else
            checkCollision(dynamic_cast<Polygon&>(a), dynamic_cast<Polygon&>(b), i, j);
    }
}

void Simulation::checkBorder(int i)
{
    float difT, difL;
    difT = figures[i]->getCentreX() + figures[i]->getBoundRadius() - image.width();
    difL = figures[i]->getCentreX() - figures[i]->getBoundRadius();

    if(difT >= 0 || difL <= 0)
    {
        if(figures[i]->getType() == 1) {
            if(difT >= 0 )
                figures[i]->setCenterX(image.width() - figures[i]->getBoundRadius());
            else
                figures[i]->setCenterX(figures[i]->getBoundRadius());
            figures[i]->setVelocityX(-figures[i]->getVelocityX());
        } else {
            Polygon *pol = dynamic_cast<Polygon*>(figures[i]);
            difT = pol->getmaxX() - image.width();
            difL = pol->getminX();
            if(difT >= 0 || difL <= 0) {
                if(difT >= 0 )
                    figures[i]->setCenterX(-difT + pol->getCentreX());
                else
                    figures[i]->setCenterX(figures[i]->getCentreX() - difL);
                figures[i]->setVelocityX(-figures[i]->getVelocityX());
            }
        }
    }

    difT = figures[i]->getCentreY() + figures[i]->getBoundRadius() - image.height();
    difL = figures[i]->getCentreY() - figures[i]->getBoundRadius();
    if(difT >= 0 || difL <= 0)
    {
        if(figures[i]->getType() == 1) {
            if(difT >= 0 )
                figures[i]->setCenterY(image.height() - figures[i]->getBoundRadius());
            else
                figures[i]->setCenterY(figures[i]->getBoundRadius());
            figures[i]->setVelocityY(-figures[i]->getVelocityY());
        } else {
            Polygon *pol = dynamic_cast<Polygon*>(figures[i]);
            difT = pol->getmaxY() - image.height();
            difL = pol->getminY();
            if(difT >= 0 || difL <= 0) {
                if(difT >= 0 )
                    figures[i]->setCenterY(-difT + pol->getCentreY());
                else
                    figures[i]->setCenterY(figures[i]->getCentreY() - difL);
                figures[i]->setVelocityY(-figures[i]->getVelocityY());
            }
        }
    }
}



float Simulation::getDif(int minA, int maxA, int minB, int maxB)
{
    float dif1 = minA - maxB,
            dif2 = minB - maxA;
    if(dif1 > dif2) return dif1;
    return dif2;
}

//float Simulation::getDif(float difT, float difL)
//{
//    if(difT >= 0) return -difT;
//    return -difL;
//}

void Simulation::drawRecImage() {
    recImage.fill(Qt::white);
    QPainter p(&recImage);

    p.setPen(QPen(Qt::black,1,Qt::SolidLine));
    p.setBrush(Qt::black);

    for(int i = 0; i < figures.size(); i++) {
        if(figures[i]->getType() == 1)
        {
            Circle* c = dynamic_cast<Circle*>(figures[i]);
            p.drawEllipse(c->getCentre(), c->getRadius(), c->getRadius());
        } else {
            Polygon* pol = dynamic_cast<Polygon*>(figures[i]);
            QPolygon poligon(pol->getCoordinates());
            p.drawPolygon(poligon);

        }
    }
}

void Simulation::drawBufPixmap(bool flag)
{
#ifndef FAST_DEBUG
    QPixmap im(bufImage.width(), bufImage.height());
    im.fill(Qt::white);
    QPainter p(&im);
    p.setPen(QPen(Qt::black,1,Qt::SolidLine));
    //    p.setBrush(Qt::blue);

    for(int i = 0; i < figures.size(); i++) {
        p.setBrush(figures[i]->getColor());
        if(figures[i]->getType() == 1)
        {
            Circle* c = dynamic_cast<Circle*>(figures[i]);
            p.drawEllipse(c->getCentre(), c->getRadius(), c->getRadius());
        } else {
            Polygon* pol = dynamic_cast<Polygon*>(figures[i]);
            QPolygon poligon(pol->getCoordinates());
            p.drawPolygon(poligon);
        }
    }

    if(drawTarget) {
        p.setPen(QPen(Qt::red,3,Qt::SolidLine));
        int x, y;
        for(int i = 0; i < listOfTatget.size(); i++) {
            x = listOfTatget[i].x();
            y = listOfTatget[i].y();
            p.drawLine(x - 7, y,x + 7, y);
            p.drawLine(x, y - 7, x, y + 7);
        }
    }



    if(flag) {
        image = bufImage;
        bufImage.swap(im);
    } else {
        image.swap(im);
        bufImage = image;
    }
    drawRecImage();

#endif
}

void Simulation::clear()
{
    figures.clear();
    image.fill(Qt::white);
    bufImage.fill(Qt::white);
    listOfTatget.clear();
}

void Simulation::checkPosition(Figure &f) {
    if(f.getType() == 1) {
        Circle &c = dynamic_cast<Circle&>(f);
        if(c.getCentreX() - c.getRadius() < 0) c.setCenterX(c.getRadius());
        if(c.getCentreX() + c.getRadius() > image.width()) c.setCenterX(image.width() - c.getRadius());
        if(c.getCentreY() - c.getRadius() < 0) c.setCenterY(c.getRadius());
        if(c.getCentreY() + c.getRadius() > image.height()) c.setCenterY(image.height() - c.getRadius());

    } else {
        Polygon &p = dynamic_cast<Polygon&>(f);
        if(p.getminX() < 0) p.setCenterX(p.getCentreX() - p.getminX());
        if(p.getmaxX() > image.width()) p.setCenterX(p.getCentreX() - image.width() - p.getmaxX());
        if(p.getminY() < 0) p.setCenterY(p.getCentreY() - p.getminY());
        if(p.getmaxX() > image.height()) p.setCenterX(p.getCentreY() - image.height() - p.getmaxY());
    }

}

QVector<QVector2D> Simulation::getNormals(QVector<QPoint> ver, QPoint center)
{
    QVector<QVector2D> normals;
    for(int i = 0; i < ver.size(); i++) {
        normals.append(QVector2D(ver[i].x() - center.x(),
                                 ver[i].y() - center.y()));
        normals[i].normalize();
    }
    return normals;
}

void Simulation::runStep(bool needDraw)
{
    setGrid();
    counter++;


    for(int i = 0; i < figures.size(); i++) {
        figures[i]->changePosition(dt);
    }

    for(int i=0; i<figures.size(); i++)
        qDebug() << i << figures[i]->getCentre() << figures[i]->getVelocity() << figures[i]->getVelocityX() << figures[i]->getVelocityY();
    qDebug();
    checkList.clear();
    float frame = gridStep / 2;
    for(int i = 0; i < figures.size(); i++) {
        if(figures[i]->getCentreX() <= frame ||
                figures[i]->getCentreX() >= image.width() - frame ||
                figures[i]->getCentreY() <= frame ||
                figures[i]->getCentreY() >= image.height() - frame) {
            checkList.append(i);
        }
    }

    for(int i = 0; i < checkList.size(); i++)
        checkBorder(checkList[i]);

    for(int i = 0; i < figures.size(); i++) {
        grid[figures[i]->getCentreY()/gridStep][figures[i]->getCentreX()/gridStep].append(i);
    }

    int row = grid.size() - 1;
    int column = grid[0].size() - 1;

    for(int i = 0; i < column; i++) {
        checkBoundTop(0, i);
    }

    for(int i = 1; i < row; i++) {
        for(int j = 0; j < column; j++) {
            checkBound(i, j);
        }
    }

    for(int i = 0; i < column; i++) {
        checkBoundBottom(row, i);
    }

    for(int i = 0; i < row; i++) {
        checkBoundRight(i, column);
    }

    checkBoundRightBottom(row, column);

    int indexA, indexB;
    for(int i = 0; i < listOfCollisions.size(); i++) {
        indexA = listOfCollisions[i].x();
        indexB = listOfCollisions[i].y();
        checkFigures(*figures[indexA], *figures[indexB], indexA, indexB);
    }
    listOfCollisions.clear();

    qSort(collisions.begin(), collisions.end(), test);
    for(int i = 0; i < collisions.size(); i++) {
        getNewSpeed(*figures[collisions[i].i], *figures[collisions[i].j], collisions[i].normal);
    }
    collisions.clear();

    float x, y;
    for(int i = 0; i < shiftVectors.size(); i++) {
        if(shiftVectors[i].size() != 0) {
            x = 0;
            y = 0;
            for(int j = 0; j < shiftVectors[i].size(); j++) {
                x += shiftVectors[i][j].normal.x() * shiftVectors[i][j].value;
                y += shiftVectors[i][j].normal.y() * shiftVectors[i][j].value;
            }
            figures[i]->changePosition(x, y);
            checkPosition(*figures[i]);

        }
    }
    shiftVectors.clear();
    shiftVectors.resize(figures.size());

    checkList.clear();
    for(int i = 0; i < figures.size(); i++) {
        if(figures[i]->getCentreX() < figures[i]->getBoundRadius() ||
                figures[i]->getCentreX() > image.width() - figures[i]->getBoundRadius() ||
                figures[i]->getCentreY() < figures[i]->getBoundRadius() ||
                figures[i]->getCentreY() > image.height() - figures[i]->getBoundRadius()) {
            checkList.append(i);
        }
    }

    if(needDraw)
        drawBufPixmap(true);
}

void Simulation::setTargetViewState(int i)
{
        drawTarget = !drawTarget;
        drawBufPixmap(false);
        emit updatePanel();
}

void Simulation::setImageSize(QSize size)
{
    recImage = QPixmap(size);
    image = QPixmap(size);
    image.fill(Qt::white);
    bufImage = QPixmap(size);
    bufImage.fill(Qt::white);
    drawBufPixmap(false);
}

int sign(float centerA, float centerB) {
    if(centerA > centerB) return 1;
    return -1;
}

void Simulation::getNewSpeed(Figure& a, Figure& b, QVector2D normal)
{
    float proj = (normal.x()*a.getVelocityX() + normal.y()*a.getVelocityY());
    QVector2D projA(normal.x()*proj, normal.y()*proj);

    proj = (normal.x()*b.getVelocityX() + normal.y()*b.getVelocityY());
    QVector2D projB(normal.x()*proj, normal.y()*proj);

    QVector2D speedPA(a.getVelocityX() - projA.x(), a.getVelocityY() - projA.y()),
            speedPB(b.getVelocityX() - projB.x(), b.getVelocityY() - projB.y());
    float sumMass = a.getMass() + b.getMass(),
            divMass = a.getMass() - b.getMass();

    QVector2D newProjA, newProjB;
    newProjA.setX((2 * b.getMass() * projB.x() + projA.x() * divMass) / sumMass);
    newProjA.setY((2 * b.getMass() * projB.y() + projA.y() * divMass) / sumMass);

    newProjB.setX((2 * a.getMass() * projA.x() - projB.x() * divMass) / sumMass);
    newProjB.setY((2 * a.getMass() * projA.y() - projB.y() * divMass) / sumMass);

    float aX = speedPA.x() + newProjA.x();
    float aY = speedPA.y() + newProjA.y();
    float bX = speedPB.x() + newProjB.x();
    float bY = speedPB.y() + newProjB.y();

    float speed = qSqrt(qPow(aX, 2) + qPow(aY, 2));
    float delta = speed - MAXSPEED;
    if(delta > 0 ) {
        float k = 1 - delta / speed;
        aX *= k;
        aY *= k;
    }
    speed = qSqrt(qPow(bX, 2) + qPow(bY, 2));
    delta = speed - MAXSPEED;
    if(speed > MAXSPEED) {
        float k = 1 - delta / speed;
        bX *= k;
        bY *= k;
    }

    a.setVelocity(QVector2D(aX, aY));
    b.setVelocity(QVector2D(bX, bY));

}

void Simulation::setShiftNormal(QPoint ca, QPoint cb, QVector2D normal,  int ia, int jb) {
    if(sign(ca.x(),cb.x()) * normal.x() > 0) {
        shiftVectors[ia][shiftVectors[ia].size() - 1].setNormal(normal);
        normal = QVector2D(-normal.x(), -normal.y());
        shiftVectors[jb][shiftVectors[jb].size() - 1].setNormal(normal);
    } else {
        shiftVectors[jb][shiftVectors[jb].size() - 1].setNormal(normal);
        normal = QVector2D(-normal.x(), -normal.y());
        shiftVectors[ia][shiftVectors[ia].size() - 1].setNormal(normal);
    }

    if(normal.x() == 0) {
        if(sign(ca.y(), cb.y()) * normal.y() > 0) {
            shiftVectors[ia][shiftVectors[ia].size() - 1].setNormal(normal);
            normal = QVector2D(-normal.x(), -normal.y());
            shiftVectors[jb][shiftVectors[jb].size() - 1].setNormal(normal);
        } else {
            shiftVectors[jb][shiftVectors[jb].size() - 1].setNormal(normal);
            normal = QVector2D(-normal.x(), -normal.y());
            shiftVectors[ia][shiftVectors[ia].size() - 1].setNormal(normal);
        }
    }
}

float countMaxX(Figure *a) {
    if(a->getType() == 1)
        return a->getCentreX() + a->getBoundRadius();
    return dynamic_cast<Polygon*>(a)->getmaxX();
}

float countMinX(Figure *a) {
    if(a->getType() == 1)
        return a->getCentreX() - a->getBoundRadius();
    return dynamic_cast<Polygon*>(a)->getminX();
}

float countMaxY(Figure *a) {
    if(a->getType() ==1)
        return a->getCentreY() + a->getBoundRadius();
    return dynamic_cast<Polygon*>(a)->getmaxY();;
}

float countMinY(Figure *a) {
    if(a->getType() ==1)
        return a->getCentreY() - a->getBoundRadius();
    return dynamic_cast<Polygon*>(a)->getminY();
}

void Simulation::checkShift(Figure* ca, Figure* cb, Shift& a, Shift& b)
{
    float delta, buf;
    float shiftAX = a.normal.x() * a.value, shiftAY = a.normal.y() * a.value,
            shiftBX = b.normal.x() * b.value, shiftBY = b.normal.y() * b.value;

    buf = countMaxX(ca) + shiftAX;
    if(buf > image.width()) {
        delta = (buf - image.width()) / shiftAX;
        a.value -= qCeil(delta * a.value);
        b.value += qCeil(delta * b.value);
    }
    buf = countMinX(ca) + shiftAX;
    if(buf < 0) {
        buf /= shiftAX;
        a.value += qCeil(buf * a.value);
        b.value -= qCeil(buf * b.value);
    }
    buf = countMaxY(ca) + shiftAY;
    if(buf > image.height()) {
        delta = (buf - image.height())/shiftAY;
        a.value -= qCeil(delta * a.value);
        b.value += qCeil(delta * b.value);
    }
    buf = countMinY(ca) + shiftAY;
    if(buf < 0) {
        buf /= shiftAY;
        a.value += qCeil(buf * a.value);
        b.value -= qCeil(buf * b.value);
    }

    buf = countMaxX(cb) + shiftBX;
    if(buf > image.width()) {
        delta = (buf - image.width()) / shiftBX;
        a.value += qCeil(delta * a.value);
        b.value -= qCeil(delta * b.value);
    }
    buf = countMinX(cb) + shiftBX;
    if(buf < 0) {
        buf /= shiftBX;
        a.value -= qCeil(buf * a.value);
        b.value += qCeil(buf * b.value);
    }
    buf = countMaxY(cb) + shiftBY;
    if(buf > image.height()) {
        delta = (buf - image.height()) / shiftBY;
        a.value += qCeil(delta * a.value);
        b.value -= qCeil(delta * b.value);
    }
    buf = countMinY(cb) + shiftBY;
    if(buf < 0) {
        buf /= shiftBY;
        a.value -= qCeil(buf * a.value);
        b.value += qCeil(buf * b.value);
    }
}

void Simulation::checkCollision(Circle& a, Circle& b,  int ia, int jb)
{
    float dif = a.getRadius() + b.getRadius() -
            qSqrt(qPow(a.getCentreX() - b.getCentreX(), 2) + qPow(a.getCentreY() - b.getCentreY(), 2));
    if(dif >= 0)
    {
        float k = a.getVelocity() + b.getVelocity();
        /*a.changePosition(ceil(-dif * a.getVelocityX() / k), ceil(-dif * a.getVelocityY() / k));
        b.changePosition(ceil(-dif * b.getVelocityX() / k), ceil(-dif * b.getVelocityY() / k));*/
        QVector2D normal(a.getCentreX()-b.getCentreX(),a.getCentreY()-b.getCentreY());
        normal.normalize();
        collisions.append(Data(ia, jb, dif, normal));
        shiftVectors[ia].append(Shift(qCeil(dif * a.getVelocity() / k)));
        shiftVectors[jb].append(Shift(qCeil(dif * b.getVelocity() / k)));
        setShiftNormal(a.getCentre(), b.getCentre(), normal, ia, jb);
        checkShift(&a, &b, shiftVectors[ia][shiftVectors[ia].size() - 1],
                shiftVectors[jb][shiftVectors[jb].size() - 1]);

        /*QVector<QVector2D> buf = getNewSpeed(a, b, normal);
        speedVectors[ia].append(buf[0]);
        speedVectors[jb].append(buf[1]);*/
    }
}

void Simulation::checkCollision(Polygon &a, Polygon &b, int ia, int jb)
{
    QVector2D normal;
    bool needTest = true;
    float maxA, maxB, minA, minB, buf, dif, minDif = -10000;
    for(int i = 0; i < a.getNormalsSize() && needTest; i++)
    {
        maxA = minA = a.getNormal(i).x() * a.getCoordinateX(0) + a.getNormal(i).y() * a.getCoordinateY(0);
        maxB = minB = a.getNormal(i).x() * b.getCoordinateX(0) + a.getNormal(i).y() * b.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = a.getNormal(i).x() * a.getCoordinateX(j) + a.getNormal(i).y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        for(int j = 1; j < b.getCoordinatesSize(); j++)
        {
            buf = a.getNormal(i).x() * b.getCoordinateX(j) + a.getNormal(i).y() * b.getCoordinateY(j);
            if(buf < minB) minB = buf;
            if(buf > maxB)  maxB = buf;
        }
        if(minA > maxB || minB > maxA)
        {
            needTest = false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = a.getNormal(i);
            }
        }
    }
    for(int i = 0; i < b.getNormalsSize() && needTest; i++)
    {
        maxA = minA = b.getNormal(i).x() * a.getCoordinateX(0) + b.getNormal(i).y() * a.getCoordinateY(0);
        maxB = minB = b.getNormal(i).x() * b.getCoordinateX(0) + b.getNormal(i).y() * b.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = b.getNormal(i).x() * a.getCoordinateX(j) + b.getNormal(i).y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        for(int j = 1; j < b.getCoordinatesSize(); j++)
        {
            buf = b.getNormal(i).x() * b.getCoordinateX(j) + b.getNormal(i).y() * b.getCoordinateY(j);
            if(buf < minB) minB = buf;
            if(buf > maxB)  maxB = buf;
        }
        if(minA > maxB || minB > maxA)
        {
            needTest = false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = b.getNormal(i);
            }
        }
    }
    QVector<QVector2D> newNornals = getNormals(a.getCoordinates(), b.getCentre());
    for(int i = 0; i < newNornals.size() && needTest; i++)
    {
        maxA = minA = newNornals[i].x() * a.getCoordinateX(0) + newNornals[i].y() * a.getCoordinateY(0);
        maxB = minB = newNornals[i].x() * b.getCoordinateX(0) + newNornals[i].y() * b.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = newNornals[i].x() * a.getCoordinateX(j) + newNornals[i].y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        for(int j = 1; j < b.getCoordinatesSize(); j++)
        {
            buf = newNornals[i].x() * b.getCoordinateX(j) + newNornals[i].y() * b.getCoordinateY(j);
            if(buf < minB) minB = buf;
            if(buf > maxB)  maxB = buf;
        }
        if(minA > maxB || minB > maxA)
        {
            needTest = false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = newNornals[i];
            }
        }
    }
    newNornals = getNormals(b.getCoordinates(), a.getCentre());
    for(int i = 0; i < newNornals.size() && needTest; i++)
    {
        maxA = minA = newNornals[i].x() * a.getCoordinateX(0) + newNornals[i].y() * a.getCoordinateY(0);
        maxB = minB = newNornals[i].x() * b.getCoordinateX(0) + newNornals[i].y() * b.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = newNornals[i].x() * a.getCoordinateX(j) + newNornals[i].y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        for(int j = 1; j < b.getCoordinatesSize(); j++)
        {
            buf = newNornals[i].x() * b.getCoordinateX(j) + newNornals[i].y() * b.getCoordinateY(j);
            if(buf < minB) minB = buf;
            if(buf > maxB)  maxB = buf;
        }
        if(minA > maxB || minB > maxA)
        {
            needTest = false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = newNornals[i];
            }
        }
    }

    if(needTest)
    {
        qDebug() << "Collision" << ia << jb;
        minDif = qAbs(minDif);
        float k = a.getVelocity() + b.getVelocity();
        /*a.changePosition(ceil(-minDif * a.getVelocityX() / k), ceil(-minDif * a.getVelocityY() / k));
        b.changePosition(ceil(-minDif * b.getVelocityX() / k), ceil(-minDif * b.getVelocityY() / k));*/
        collisions.append(Data(ia, jb, minDif, normal));
        shiftVectors[ia].append(Shift(qCeil(minDif * a.getVelocity() / k)));
        shiftVectors[jb].append(Shift(qCeil(minDif * b.getVelocity() / k)));
        setShiftNormal(a.getCentre(), b.getCentre(), normal, ia, jb);
        checkShift(&a, &b, shiftVectors[ia][shiftVectors[ia].size() - 1],
                shiftVectors[jb][shiftVectors[jb].size() - 1]);
        /*
        QVector<QVector2D> buf = getNewSpeed(a, b, normal);
        speedVectors[ia].append(buf[0]);
        speedVectors[jb].append(buf[1]);*/
    }
}

void Simulation::checkCollision(Polygon &a, Circle &b, int ia, int jb)
{
    QVector2D normal;
    bool needTest = true;
    float maxA, maxB, minA, minB, buf, minDif = -10000, dif;
    for(int i = 0; i < a.getNormalsSize() && needTest; i++)
    {
        maxA = minA = a.getNormal(i).x() * a.getCoordinateX(0) + a.getNormal(i).y() * a.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = a.getNormal(i).x() * a.getCoordinateX(j) + a.getNormal(i).y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        buf = a.getNormal(i).x() * b.getCentreX() + a.getNormal(i).y() * b.getCentreY();
        minB = buf - b.getRadius();
        maxB = buf + b.getRadius();
        if(minA > maxB || minB > maxA)
        {
            needTest= false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = a.getNormal(i);
                qDebug() << i;
            }
        }
    }
    QVector<QVector2D> newNornals = getNormals(a.getCoordinates(), b.getCentre());
    for(int i = 0; i < newNornals.size() && needTest; i++)
    {
        maxA = minA = newNornals[i].x() * a.getCoordinateX(0) + newNornals[i].y() * a.getCoordinateY(0);

        for(int j = 1; j < a.getCoordinatesSize(); j++)
        {
            buf = newNornals[i].x() * a.getCoordinateX(j) + newNornals[i].y() * a.getCoordinateY(j);
            if(buf < minA) minA = buf;
            if(buf > maxA)  maxA = buf;
        }
        buf = newNornals[i].x() * b.getCentreX() + newNornals[i].y() * b.getCentreY();
        minB = buf - b.getRadius();
        maxB = buf + b.getRadius();
        if(minA > maxB || minB > maxA)
        {
            needTest= false;
            break;
        }
        else {
            dif  = getDif(minA, maxA, minB, maxB);
            if(dif > minDif) {
                minDif = dif;
                normal = newNornals[i];
                qDebug() << i;
            }
        }
    }
    if(needTest)
    {
        minDif = qAbs(minDif);
        float k = a.getVelocity() + b.getVelocity();
        /*a.changePosition(ceil(-minDif * a.getVelocityX() / k), ceil(-minDif * a.getVelocityY() / k));
        b.changePosition(ceil(-minDif * b.getVelocityX() / k), ceil(-minDif * b.getVelocityY() / k));*/
        collisions.append(Data(ia, jb, minDif, normal));
        shiftVectors[ia].append(Shift(qCeil(minDif * a.getVelocity() / k)));
        shiftVectors[jb].append(Shift(qCeil(minDif * b.getVelocity() / k)));
        setShiftNormal(a.getCentre(), b.getCentre(), normal, ia, jb);
        checkShift(&a, &b, shiftVectors[ia][shiftVectors[ia].size() - 1],
                shiftVectors[jb][shiftVectors[jb].size() - 1]);
        /*
        QVector<QVector2D> buf = getNewSpeed(a, b, normal);
        speedVectors[ia].append(buf[0]);
        speedVectors[jb].append(buf[1]);*/
    }
}
