#ifndef CLUSTER_H
#define CLUSTER_H

#include <QImage>

#define AnglesTotal 360
#define MinRadiusDistance 2
#define WinFullDebug

struct Pixel
{
    /*  Coords - vector of pixel's orthogonal coordinates.
        R, Angles - polar coordinates of the pixel.
        Next, Prev - pointers to another pixels in the list.
        DNext - pointer to another pixel in the degrees list.
        ONext - pointer to another pixel in the normal order list.*/
    QVector<int> Coords;
    double R;
    QVector<double> Angles;
    Pixel* Next;
    Pixel* Prev;
};

class Cluster
{

    int OrthDimensionCount;
    QVector<int> Borders;
    QVector<double> CMass;
    double *Otkls;
    double MaxOtkl;
    int MaxOtklAngle;
    double DistMean;

    int PixCount;

    QVector<double> R360;
    double R360Length;
    double R360Width;


    Pixel *PLast;
    Cluster *Next;
    Cluster *Prev;

public:

    Pixel *PFirst;
    double RMax;

    Cluster();
    Cluster(int OrthDimensionCountNew, QVector<int> Coords);
    ~Cluster();
    void Initialize(int OrthDimensionCountNew, const QVector<int> &CoordsNew,
                    Cluster* NextNew, Cluster* PrevNew);
    void ScanImage(QImage *image);
    void AllCalculateCMass(void);
    void CalculateCMass(int Offset, int DimensionCount);
    Cluster* GetNextCluster(void);
    int GetPixCount(void);
    void AllToPol(void);
    void ToPol(int Offset, int DimensionCount);
    void AllPolarSort(void);
    void PolarSort(int Offset, int DimensionCount);
    void AllInterDegNew(int AnglesTot);
    void InterDegNew(int AnglesTot, int Offset, int DimensionCount);
    void AllNorm(int AnglesTot);
    void Norm(int AnglesTot);
    void Clear(void);
    void AddPixel(QVector<int> CoordsNew);
    void Copy(Cluster* Dest);
    void AddCluster(int OrthDimensionCountNew, QVector<int> CoordsNew);
    void AddCluster(Cluster *Src);
    int GetOrthDimensionCount(void);
    double GetR360(int i);
    void SetR360(int i, double x);

    QPoint getCMass();


};

#endif // CLUSTER_H
