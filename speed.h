#ifndef SPEED_H
#define SPEED_H

class Speed
{
    int x;
    int y;
    double value;
public:
    Speed();
    Speed(int x, int y);
    int getX() { return x;}
    int getY() { return y;}
    double getValue() { return value;}
    void setX(int x) { this->x = x;}
    void setY(int y) { this->y = y;}
    void updateValue();

};

#endif // SPEED_H
