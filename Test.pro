QT       += core gui
QT       -= gl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Test
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

DEFINES += "QT_NO_DEBUG_OUTPUT=1"
#DEFINES += "FAST_DEBUG=1"
SOURCES += main.cpp\
        mainwindow.cpp \
    figure.cpp \
    speed.cpp \
    circle.cpp \
    polygon.cpp \
    simulation.cpp \
    panel.cpp \
    sample.cpp \
    objectfinder.cpp \
    cluster.cpp

HEADERS  += mainwindow.h \
    figure.h \
    speed.h \
    circle.h \
    polygon.h \
    simulation.h \
    panel.h \
    sample.h \
    objectfinder.h \
    cluster.h

