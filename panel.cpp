
#include "panel.h"
#include <QPainter>
#include <QDrag>
#include <QDebug>
#include <QMimeData>

Panel::Panel(bool enabled, int type, QWidget *parent) :
    QWidget(parent), dropEnabled(enabled), type(type)
{
    setAcceptDrops(true);
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

void Panel::setDropEnavled(bool enabled)
{
    dropEnabled = enabled;
    if(enabled)
        setCursor(Qt::ArrowCursor);
    else
        setCursor(Qt::OpenHandCursor);
}

void Panel::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.drawPixmap(0, 0, map);
}

void Panel::mousePressEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton && !dropEnabled)
    {
        QPixmap pixmap = dragMap;

        QByteArray itemData;
        QDataStream dataStream(&itemData, QIODevice::WriteOnly);

        dataStream << pixmap << event->localPos();

        QMimeData *mimeData = new QMimeData;
        mimeData->setData("pixmap", itemData);

        QDrag *drag = new QDrag(this);
        drag->setMimeData(mimeData);
        drag->setHotSpot(QPoint(pixmap.width() / 2, pixmap.height() / 2));
        drag->setPixmap(pixmap);

        if (!(drag->exec(Qt::MoveAction) == Qt::MoveAction)) {
            //отрисовать на другой панели?
        }
    }
}

void Panel::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("pixmap") && dropEnabled)
    {
        QByteArray pieceData = event->mimeData()->data("pixmap");
        QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
        QPixmap pixmap;
        QPoint location;
        dataStream >> pixmap >> location;

        event->setDropAction(Qt::MoveAction);
        event->accept();
        qDebug() << event->pos();
        emit addFigure(QVector2D(event->pos().x(), event->pos().y()));
        emit setEnableTrue();
    } else {
        event->ignore();
    }
}

void Panel::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("pixmap"))
        event->accept();
    else
        event->ignore();
}

void Panel::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->mimeData()->hasFormat("pixmap"))
    {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else {
        event->ignore();
    }
}

void Panel::resizeEvent(QResizeEvent *event)
{
    if(type == 0)
    {
    map = QPixmap(event->size());
    emit sizeChanged(event->size());
    }
}

