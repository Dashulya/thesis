#ifndef POLYGON_H
#define POLYGON_H
#include"figure.h"
#include <QVector>
#include <QVector2D>
#include <QPoint>

class Polygon : public Figure
{
    int minX, maxX, minY,maxY;
    QVector<QPoint> coordinates;
    QVector<QVector2D> normals;
    void setNormals();
    void setMass();

    inline float area(QPoint a, QPoint b);

public:
    Polygon(QVector<QPoint> c, QVector2D velocity, QVector2D center);
    ~Polygon(){}
    void addCoordinate(QVector2D coordinate);
    int getNormalsSize() { return normals.size();}
    int getCoordinatesSize() { return coordinates.size();}
    QVector2D getNormal(int i) { return normals[i];}//нужна ли проверка?
    float getCoordinateX(int i) { return coordinates[i].x() + center.x();}
    float getCoordinateY(int i) { return coordinates[i].y() + center.y();}
    QVector<QPoint> getCoordinates();
    int getType() { return 2;}
    float getmaxX();
    float getminX();
    float getmaxY();
    float getminY();
};

#endif // POLYGON_H
