#include "sample.h"

Sample::Sample(int radius, QPixmap map)
    :type(1), radius(radius), map(map)
{
}

Sample::Sample(QVector<QPoint> coordinates, QPixmap map)
    :type(2), coordinates(coordinates), map(map)
{
}
