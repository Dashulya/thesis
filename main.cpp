#include "mainwindow.h"
#include <QApplication>
#include <QSet>
#include <iostream>
#include <QMetaType>

int main(int argc, char *argv[])
{
    qRegisterMetaType<QVector<QPoint>>("QVector<QPoint>");
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
