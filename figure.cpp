#include "figure.h"
#include <QDebug>
float Figure::getVelocity()
{
    return sqrt(pow(velocity.x(), 2) + pow(velocity.y(), 2));
}

void Figure::changePosition(float dt)
{
    center.setX(center.x() + dt * velocity.x());
    center.setY(center.y() + dt * velocity.y());
}

void Figure::changePosition(float dx, float dy)
{
    center.setX(center.x() + dx);
    center.setY(center.y() + dy);
}
