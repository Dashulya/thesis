#include "circle.h"
#include <QtMath>

Circle::Circle(QVector2D centre, QVector2D speed, int radius)
    :Figure(centre, speed), radius(radius)
{
    setBoundRadius(radius);
    setMass();
}

void Circle::setMass()
{
    mass = M_PI*pow(radius, 2);
}
