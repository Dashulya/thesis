#include "cluster.h"
#include <QRgb>
#include <QDebug>



Cluster::Cluster()
{
    Initialize(0, QVector<int>(), NULL, NULL);
}
Cluster::Cluster(int OrthDimensionCountNew, QVector<int> Coords)
{
    Initialize(OrthDimensionCountNew, Coords, NULL, NULL);
}
Cluster::~Cluster()
{
    Pixel* CurrP;
    CurrP=PLast;
    while(CurrP!=PFirst)
    {
        CurrP=CurrP->Prev;
        delete CurrP->Next;
    }
    delete CurrP;
    if(Otkls!=NULL) delete[] Otkls;
    if(Prev!=NULL) Prev->Next=Next;
    if(Next!=NULL) Next->Prev=Prev;
}

void Cluster::Initialize(int OrthDimensionCountNew, const QVector<int> &CoordsNew,
                         Cluster* NextNew, Cluster* PrevNew)
{
    int i;
    OrthDimensionCount=OrthDimensionCountNew;

    if(OrthDimensionCount>0)
    {
        Borders=QVector<int>(OrthDimensionCountNew*2);
        CMass=QVector<double> (OrthDimensionCountNew);
    }

    for(i=0; i<OrthDimensionCount; i++)
    {
        Borders[i*2]=CoordsNew[i];
        Borders[i*2+1]=CoordsNew[i];
        CMass[i]=0;
    }
    PFirst=(Pixel*)new Pixel;
    if(OrthDimensionCount>0)
    {
        PFirst->Coords=QVector<int>(OrthDimensionCount);
        PFirst->Angles=QVector<double>(OrthDimensionCount-1);
    }
    PLast=PFirst;
    PFirst->R=0;
    for(i=0; i<OrthDimensionCount-1; i++)
        PFirst->Angles[i]=0;
    for(i=0; i<OrthDimensionCount; i++)
        PFirst->Coords[i]=CoordsNew[i];
    PFirst->Next=NULL;
    PFirst->Prev=NULL;
    Next=NextNew;
    Prev=PrevNew;
    if(NextNew!=NULL) NextNew->Prev=this;
    if(PrevNew!=NULL) PrevNew->Next=this;
    PixCount=1;
    R360Length=-1;
    R360Width=-1;
    Otkls=NULL;
    MaxOtkl=-1;
    MaxOtklAngle=-1;
}
void Cluster::ScanImage(QImage *image)
{
    int x, y, i;
    QVector<int> CoordsNew(2);
       QRgb *ptr; //Указатель на графические данные
       QVector<Cluster*> top(image->width()+2), bottom(image->width()+2);
       Cluster *tmp;
//       top=new Cluster*[image->width()+2]; //Верхняя строка указателей на кластеры
//       bottom=new Cluster*[image->width()+2]; //Нижняя строка указателей на кластеры

       //Заполняем строки указателей
       for(x=0; x<image->width()+2; x++)
          {
          top[x]=NULL;
          bottom[x]=NULL;
          }

       //Цикл по строкам изображения
       for(y=0; y<image->height(); y++)
          {
          //Указатель на следующую строку
          ptr=(QRgb*)image->scanLine(y);

          //Цикл по пикселам в строке
          for(x=1; x<image->width()+1; x++)
             {
             //Проверяем только 1 пиксел из 3х
              qDebug() << ptr[x-1];
             if(qRed(ptr[x-1]) == 255)
                //Он черный -> ищем, в какой кластер его добавить (возможно, в новый)
                {
                CoordsNew[0]=x-1;
                CoordsNew[1]=y;

                if(bottom[x-1]!=NULL)
                   {
                   bottom[x-1]->AddPixel(CoordsNew);
                   bottom[x]=bottom[x-1];
                   }
                else if(top[x-1]!=NULL)
                   {
                   top[x-1]->AddPixel(CoordsNew);
                   bottom[x]=top[x-1];
                   }
                else if(top[x]!=NULL)
                   {
                   top[x]->AddPixel(CoordsNew);
                   bottom[x]=top[x];
                   }
                else if(top[x+1]!=NULL)
                   {
                   top[x+1]->AddPixel(CoordsNew);
                   bottom[x]=top[x+1];
                   }
                else
                   {
                   this->AddCluster(2, CoordsNew);
                   bottom[x]=this->Next;
                   }
                }
             }

          //Цикл на объединение кластеров.
          for(x=1; x<image->width()+1; x++)
             {
             if((bottom[x]!=NULL) && (top[x+1]!=NULL) && (bottom[x]!=top[x+1]))
                {
                tmp=top[x+1];
                bottom[x]->Copy(tmp);
                for(i=1; i<image->width()+1; i++)
                   {
                   if(top[i]==tmp)
                      top[i]=bottom[x];
                   if(bottom[i]==tmp)
                      bottom[i]=bottom[x];
                   }
                tmp->~Cluster();
                }
             else if((bottom[x+1]!=NULL) && (top[x]!=NULL) && (bottom[x+1]!=top[x]))
                {
                tmp=top[x];
                bottom[x+1]->Copy(tmp);
                for(i=1; i<image->width()+1; i++)
                   {
                   if(top[i]==tmp)
                      top[i]=bottom[x+1];
                   if(bottom[i]==tmp)
                      bottom[i]=bottom[x+1];
                   }
                tmp->~Cluster();
                }
             }

          //Переходим на следующую строку - копируем данные из 2го буфера в 1й.
          for(x=1; x<image->width()+1; x++)
             {
             top[x]=bottom[x];
             bottom[x]=NULL;
             }
          }
//       delete[] top;
//       delete[] bottom;
}

void Cluster::AllCalculateCMass(void)
{
    Cluster *CurrC=this->Next;
    while(CurrC!=NULL)
    {
        CurrC->CalculateCMass(0, 2);
        CurrC=CurrC->Next;
    }
}

void Cluster::CalculateCMass(int Offset, int DimensionCount)
{
    int i;
    Pixel* CurrP=PFirst;
    for(i=Offset; i<Offset+DimensionCount; i++)
        CMass[i]=0;
    while(CurrP!=NULL)
    {
        for(i=Offset; i<Offset+DimensionCount; i++) {
            CMass[i]+=CurrP->Coords[i];
            qDebug() << CurrP->Coords[i] << i;
        }
        CurrP=CurrP->Next;
    }
    for(i=Offset; i<Offset+DimensionCount; i++) {
        CMass[i]/=(double)PixCount;
    }
}

Cluster* Cluster::GetNextCluster(void)
{
    return Next;
}

int Cluster::GetPixCount(void)
{
    return PixCount;
}

void Cluster::AllToPol(void)
{
    Cluster *CurrC=this->Next;
    while(CurrC!=NULL)
    {
        CurrC->ToPol(0, 2);
        CurrC=CurrC->Next;
    }
}

void Cluster::ToPol(int Offset, int DimensionCount)
{
    int i;
    double Val, C1, C2, Pi=3.14159;
    Pixel *CurrP=PFirst;
    while(CurrP!=NULL)
    {
        Val=0;
        for(i=Offset; i<Offset+DimensionCount-1; i++)
        {
            C1=(double)CurrP->Coords[i]-CMass[i];
            C2=(double)CurrP->Coords[i+1]-CMass[i+1];
            Val+=C1*C1;
            if(C1==0)
            {
                if(C2>0)
                    CurrP->Angles[i]=Pi/2;
                else
                    CurrP->Angles[i]=3*Pi/2;
            }
            else if(C1>0)
            {
                CurrP->Angles[i]=atan(C2/C1);
                if(C2<0)
                    CurrP->Angles[i]+=2*Pi;
            }
            else
                CurrP->Angles[i]=Pi+atan(C2/C1);
        }
        Val+=C2*C2;
        CurrP->R=pow(Val, ((double)1)/((double)(DimensionCount-Offset)));
        CurrP=CurrP->Next;
    }
}

void Cluster::AllPolarSort(void)
{
    Cluster *CurrC=this->Next;
    while(CurrC!=NULL)
    {
        CurrC->PolarSort(0, 2);
        CurrC=CurrC->Next;
    }
}

void Cluster::PolarSort(int Offset, int DimensionCount)
{
    bool NoChange=false;
    int i;
    Pixel *CurrP, *HelpP;
    while(NoChange!=true)
    {
        NoChange=true;
        CurrP=PFirst;
        while(CurrP->Next!=NULL)
        {
            for(i=Offset; i<Offset+DimensionCount-1; i++)
            {
                if(CurrP->Next->Angles[i]>CurrP->Angles[i])
                {
                    NoChange=false;
                    HelpP=CurrP->Next;
                    CurrP->Next=HelpP->Next;
                    HelpP->Prev=CurrP->Prev;
                    if(CurrP->Next!=NULL)
                        CurrP->Next->Prev=CurrP;
                    else
                        PLast=CurrP;
                    if(HelpP->Prev!=NULL)
                        HelpP->Prev->Next=HelpP;
                    else
                        PFirst=HelpP;
                    CurrP->Prev=HelpP;
                    HelpP->Next=CurrP;
                    break;
                }
                else if(CurrP->Next->Angles[i]==CurrP->Angles[i])
                {
                    if(i!=Offset+DimensionCount-2)
                        continue;
                    if(CurrP->Next->R>CurrP->R)
                    {
                        NoChange=false;
                        HelpP=CurrP->Next;
                        CurrP->Next=HelpP->Next;
                        HelpP->Prev=CurrP->Prev;
                        if(CurrP->Next!=NULL)
                            CurrP->Next->Prev=CurrP;
                        else
                            PLast=CurrP;
                        if(HelpP->Prev!=NULL)
                            HelpP->Prev->Next=HelpP;
                        else
                            PFirst=HelpP;
                        CurrP->Prev=HelpP;
                        HelpP->Next=CurrP;
                        break;
                    }
                    else
                        CurrP=CurrP->Next;
                }
                else
                    CurrP=CurrP->Next;
            }
        }
    }
}

void Cluster::AllInterDegNew(int AnglesTot)
{
    Cluster *CurrC=this->Next;
    while(CurrC!=NULL)
    {
        CurrC->InterDegNew(AnglesTot, 0, 2);
        CurrC=CurrC->Next;
    }
}

void Cluster::InterDegNew(int AnglesTot, int Offset, int DimensionCount)
{
    bool miss=false;
    int i, j, k, r, val;
    double x1, y1, x2, y2, xf, yf, k1, b1, k2, b2;
    double Pi=3.14159, Const=((double)AnglesTot)/(Pi*2);
    Pixel *CurrP=PFirst;

    R360=QVector<double>(AnglesTot);
    for(i=0; i<AnglesTot; i++)
        R360[i]=(-1);

    while(CurrP!=NULL)
    {
        val=(int)(CurrP->Angles[Offset]*Const);
        if(((double)(CurrP->Angles[Offset]*Const)-(double)val)>0.5)
            val+=1;
        if(val>359)
            val=0;
        if(R360[val]==-1)
            R360[val]=CurrP->R;

        else if(CurrP->R>R360[val])
            R360[val]=CurrP->R;
        CurrP=CurrP->Next;
    }

    k=0;
    if(R360[0]==(-1)||R360[AnglesTot-1]==(-1))
    {
        for(i=0; i<AnglesTot; i++)
        {
            if(R360[i]!=(-1))
                break;
            k+=1;
        }
        if(i>=AnglesTot)
            return;

        for(j=AnglesTot-1; j>=0; j--)
        {
            if(R360[j]!=(-1))
                break;
            k+=1;
        }
        if(j<0)
            return;

        x1=R360[i]*cos(((double)i)/Const);
        y1=R360[i]*sin(((double)i)/Const);
        x2=R360[j]*cos(((double)j)/Const);
        y2=R360[j]*sin(((double)j)/Const);
        k1=(y2-y1)/(x2-x1);
        b1=y1-k1*x1;

        for(r=j+1; r<AnglesTot; r++)
        {
            k2=tan(((double)r)/Const);
            b2=0;

            xf=(b2-b1)/(k1-k2);
            yf=k1*xf+b1;

            R360[r]=sqrt(xf*xf+yf*yf);
        }
        for(r=0; r<i; r++)
        {
            k2=tan(((double)r)/Const);
            b2=0;

            xf=(b2-b1)/(k1-k2);
            yf=k1*xf+b1;

            R360[r]=sqrt(xf*xf+yf*yf);
        }
    }

    for(i=0; i<AnglesTot; i++)
    {
        if(miss==false)
        {
            if(R360[i]!=(-1))
                continue;
            else
            {
                miss=true;
                j=i-1;
                k=1;
            }
        }
        else
        {
            if(R360[i]!=(-1))
            {
                miss=false;

                x1=R360[i]*cos(((double)i)/Const);
                y1=R360[i]*sin(((double)i)/Const);
                x2=R360[j]*cos(((double)j)/Const);
                y2=R360[j]*sin(((double)j)/Const);
                k1=(y2-y1)/(x2-x1);
                b1=y1-k1*x1;

                for(r=1; r<=k; r++)
                {
                    k2=tan(((double)(r+j))/Const);
                    b2=0;

                    xf=(b2-b1)/(k1-k2);
                    yf=k1*xf+b1;

                    R360[r+j]=sqrt(xf*xf+yf*yf);
                }
            }
            else
            {
                k+=1;
            }
        }
    }
}

void Cluster::AllNorm(int AnglesTot)
{
    Cluster *CurrC=this->Next;
    while(CurrC!=NULL)
    {
        CurrC->Norm(AnglesTot);
        CurrC=CurrC->Next;
    }
}

void Cluster::Norm(int AnglesTot)
{
    Cluster *CurrC=this; //->Next;
    CurrC->RMax=0;
    for (int i=0; i<AnglesTot; i++)
    {
        if (CurrC->RMax < R360[i]) CurrC->RMax=R360[i];
    }
    for (int i=0; i<AnglesTot; i++)
    {
        R360[i] /= CurrC->RMax;
    }
}

void Cluster::Clear(void)
{
    while(Next!=NULL)
        Next->~Cluster();
}

void Cluster::AddPixel(QVector<int> CoordsNew)
{
    int i;
    Pixel* CurrP;
    CurrP=PFirst;
    CurrP=(Pixel*)new Pixel;
    if(OrthDimensionCount>0)
    {
        CurrP->Coords=QVector<int>(OrthDimensionCount);
        CurrP->Angles=QVector<double>(OrthDimensionCount-1);
    }
    CurrP->R=0;
    for(i=0; i<OrthDimensionCount-1; i++)
        CurrP->Angles[i]=0;
    for(i=0; i<OrthDimensionCount; i++)
        CurrP->Coords[i]=CoordsNew[i];
    CurrP->Next=PFirst;
    CurrP->Prev=NULL;
    PFirst=CurrP;
    CurrP->Next->Prev=CurrP;
    for(i=0; i<OrthDimensionCount; i++)
    {
        if(Borders[i*2]>CoordsNew[i])
            Borders[i*2]=CoordsNew[i];
        if(Borders[i*2+1]<CoordsNew[i])
            Borders[i*2+1]=CoordsNew[i];
    }
    ++PixCount;
}

void Cluster::Copy(Cluster* Dest)
{
    Pixel* CurrP;
    CurrP=Dest->PFirst;
    while(CurrP!=NULL)
    {
        AddPixel(CurrP->Coords);
        CurrP=CurrP->Next;
    }
}

void Cluster::AddCluster(int OrthDimensionCountNew, QVector<int> CoordsNew)
{
    Cluster* NewCl;
    NewCl=(Cluster*)new Cluster;
    NewCl->Initialize(OrthDimensionCountNew, CoordsNew, Next, this);
}

void Cluster::AddCluster(Cluster *Src)
{
    Pixel* CurrP;
    Cluster* NewCl;
    NewCl=(Cluster*)new Cluster;
    CurrP=Src->PFirst;
    NewCl->Initialize(Src->GetOrthDimensionCount(), CurrP->Coords, Next, this);

    CurrP=Src->PFirst;
    CurrP=CurrP->Next;

    while(CurrP!=NULL)
    {
        NewCl->AddPixel(CurrP->Coords);
        CurrP=CurrP->Next;
    }
}

int Cluster::GetOrthDimensionCount(void)
{
    return OrthDimensionCount;
}

double Cluster::GetR360(int i)
{
    return R360[i];
}

void Cluster::SetR360(int i, double x)
{
    R360[i] = x;
}

QPoint Cluster::getCMass()
{
    return QPoint(CMass[0], CMass[1]);
}
